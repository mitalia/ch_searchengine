# Calvin & Hobbes Search Engine #

A really simple Flask-based Calvin & Hobbes search engine. You can see it online at http://mitalia.dns-dns.com/ch_se.

The inspiration came from [the other major C&H search engine](http://michaelyingling.com/random/calvin_and_hobbes/), which however only lets you search for whole consecutive strings of text (`LIKE '%your text%'`), instead of the more natural "bag of words" search.

The data was taken by the [now-deleted "Calvin Quotes" document](http://www.scribd.com/doc/6749712/Calvin-Quotes) on Scribd, probably by [S Anand](http://www.s-anand.net/).

Since the data to index is quite small, it's re-indexed at each start (a pickled cache proved to be only marginally faster).

Each field of `script.fsv`[1] is "baggized" (split on punctuation & co. to a bag of lowercase words), and each word is stored in the `Index` instance, which contains a `dict` from word name to a list of post IDs where it appears. Then, the lookup is just a matter of doing an AND between the lists of each word of the query (plus sorting by words frequency).

## License ##

Standard 3-clause BSD (see LICENSE)

---

1. `script.fsv` is composed of three fields, each separated by the standard ASCII record separator (RS) character (AKA character 0x1E, AKA `^^`); I'm always baffled by the fact that people use commas or semicolons for delimited text (and then have to reinvent convoluted escaping schemes) when ASCII already provides *four* non-printable characters for field delimitation (from 0x1B to 0x1F, in reverse order of detail).