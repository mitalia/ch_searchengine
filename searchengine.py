import re

class Index:
    def __init__(self, index={}):
        self.index = index

    def addBag(self, idx, bag):
        for w in bag:
            bucket = self.index.setdefault(w, {})
            bucket[idx] = bucket.get(idx, 0)+1

    def getMatches(self, bag):
        results = None
        for w in bag:
            r = self.index.get(w,{})
            if results is None:
                results = r
            else:
                r2 = {}
                for k,v in results.iteritems():
                    v2 = r.get(k, 0)
                    if v2==0:
                        continue
                    r2[k]=v+v2
                results = r2
            if len(results)==0:
                break
        return results or {}

def baggize(text):
    return [s.lower() for s in re.split('\W+', text) if len(s)>2]
