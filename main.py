#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from flask import Flask, request, render_template, redirect, url_for
import searchengine as se
import cPickle
import os

app = Flask(__name__)
app.jinja_env.trim_blocks=True
app.jinja_env.lstrip_blocks=True

index = se.Index()
fullText = {}
scriptsFile = os.path.dirname(os.path.realpath(__file__)) + '/scripts.fsv'

with open(scriptsFile, "r") as f:
    for l in f:
        k,quote,desc = l.strip().split(chr(30))
        k = int(k)
        fullText[k] = (quote, desc)
        index.addBag(k, se.baggize(quote))

@app.route('/')
def root():
    return redirect(url_for('searchPage'))

class CHEntry:
    def __init__(self, idx, relevance):
        self.idx = idx
        self.relevance = relevance
        idxs = str(idx)
        yy = "19" + idxs[0:2]; mm=idxs[2:4]; dd=idxs[4:6]
        self.date = "%s/%s/%s" % (dd, mm, yy)
        self.link = "http://www.gocomics.com/calvinandhobbes/%s/%s/%s" % (yy, mm, dd)

    def getQuote(self):
        return fullText[self.idx][0]

    def getDescription(self):
        return fullText[self.idx][1]

@app.route('/search')
def searchPage():
    query = request.args.get('query', '')
    m = []
    if query != '':
        w = se.baggize(query)
        m = [
                CHEntry(e[0], e[1])
                for e in sorted(
                    index.getMatches(w).iteritems(),
                    key=lambda e: -e[1])
                ]
    return render_template('search.html', query=query, entries=m)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
